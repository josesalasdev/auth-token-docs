Welcome to Auth Token's documentation!
====================================================

Package to generate jwt token.

Get the code
------------

The `source <https://gitlab.com/developerjoseph/auth-token>`_ is available on GitLab. 

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:

    install
    userguide


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
