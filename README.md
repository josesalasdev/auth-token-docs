# Auth Token Jwt

Package to generate token Jwt


## Package installation
- Installation
    ```shell
    $ pip3 install auth-token
    ```

## HTML
- Create html
    ```shell
    $ make html
    ```

- View html
    ./build/html/index.html

## Run in docker
```shell
$ docker-compose up --build
```

## Refs:
- [Sphinx](http://www.sphinx-doc.org/en/master/)
- [Theme](https://sphinx-themes.org/html/sphinx-glpi-theme/glpi/index.html)
- [httpdomain](https://sphinxcontrib-httpdomain.readthedocs.io/en/stable/)
