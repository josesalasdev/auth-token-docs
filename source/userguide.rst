User Guide
==========

Here you will learn how to use the package correctly and you will see useful examples


Encode
------

Generate jwt token.

Example::

    >>> from auth_token import AuthToken

    >>> identifier = '12345678'
    >>> exp_minutes = 60
    >>> secret_key = 'secret_key'
    >>> auth = AuthToken(identifier, exp_minutes, secret_key)
    >>> response = auth.encode()
    >>> response.token
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODc5NTY5ODQsImlhdCI6MTU4Nzk1NjY4NCwic3ViIjoiMSIsInV1aWQiOiJhYTUwNWE3ZS1hMjEyLTRiOTktYmI3Yy02Njg3MjViZGQ3YTcifQ.l1uGXiZpZHuOt0iWmcksLsdkUQjYesH_OxmCpjJHWDk'

Decode
------

Decode jwt token.

Example::

    >>> from auth_token import AuthToken

    >>> identifier = '12345678'
    >>> exp_minutes = 60
    >>> secret_key = 'secret_key'
    >>> auth = AuthToken(identifier, exp_minutes, secret_key)
    >>> token = 'token_string'
    >>> payload = auth.decode(token)
    >>> payload
    <auth_token.utils.Payload object at 0x7f7078affbd0>
    >>> payload.error
    'invalid_token'
